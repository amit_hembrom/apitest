package src.test;

import java.io.IOException;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CompareJSON {
    private static final Logger logger = LoggerFactory.getLogger(CompareJSON.class);

    public static void main(String[] args) {
        final ObjectMapper mapper = new ObjectMapper();

        JSONObject obj1 = new JSONObject();
        obj1.put("aa", 2);
        obj1.put("b","ABCD");
        JSONObject obj2 = new JSONObject();
        
        obj2.put("b","ABCD");
        obj2.put("aa", 2);
        
        JsonNode tree1;
        try {
            tree1 = mapper.readTree(obj1.toString());
            final JsonNode tree2 = mapper.readTree(obj2.toString());
            System.out.println(tree1.equals(tree2) );
        } catch (JsonProcessingException e) {
            logger.error("",e);
        } catch (IOException e) {
            logger.error("",e);
        }
        
    }

}
