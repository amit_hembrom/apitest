package com.games24x7.api;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

public class PostActions implements Runnable  {

    private String orderIdValue;
    private long userId;
    
    
    public PostActions(String orderId,Long uId) {
        this.orderIdValue = orderId;
        this.userId = uId;
    }

    public void run() {
        try {
            sendPostRequest();
        } catch (UnsupportedEncodingException e) {
            System.out.println( "Exception "+e);
        }
    }

    private void sendPostRequest() throws UnsupportedEncodingException {

        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost("http://10.14.24.155:8084/bs/api/bonus/issueBonus");

        StringEntity params = new StringEntity("{\"userId\":"+userId+",\"promoCode\":\"PGIS-616_test_2\",\"orderId\":\""+orderIdValue+"\",\"depositAmt\":25,\"channelId\":1,\"type\":\"insert\"}");
       
        //StringEntity params = new StringEntity("{\"userId\":1530,\"orderId\":\""+orderIdValue+"\",\"paymentOption\":3,\"paymentGateway\":2,\"noOfTimesDeposited\":2,\"depositAmt\":25,\"channelId\":1,\"type\":\"update\"}");

        post.setHeader("Accept", "application/json");
        post.setHeader("Content-type", "application/json");
        try {
            post.setEntity(params);
            HttpResponse response = client.execute(post);

            // Print out the response message
            System.out.println(EntityUtils.toString(response.getEntity()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
