package com.games24x7.actions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.games24x7.beans.InputAPI;

@Service
public class ReadJSON {

    public InputAPI fileToAPOJO(String inputJSON){
        ObjectMapper objectMapper = new ObjectMapper();
        InputAPI emp=null;
        try {
            emp = objectMapper.readValue(new File(inputJSON), InputAPI.class);
        } catch (Exception e) {
            System.out.println( "Error in jsonToInputJSON "+e);
        }
        return emp;
    }
    public ArrayList<InputAPI> jsonToInputJSON(String inputJSON){
        ArrayList<InputAPI> apiList = new ArrayList<InputAPI>();
        String[] strDiff = inputJSON.split("NEXT");
        for(int i=0;i<strDiff.length;i++){
           InputAPI api = getAPIPOJO(strDiff[i]);
           apiList.add(api);
        }
        
        return apiList;
     }
    public InputAPI getAPIPOJO(String string) {
        InputAPI input=new InputAPI();
        String[] str = string.split("\\$");
        for(int i=0;i<str.length;i++){
            
            if(i==0){
                //System.out.println("STR 0 = "+str[i].substring(0,str[i].lastIndexOf(","))+"}");
                String stringBody =str[i].substring(0,str[i].lastIndexOf(","))+"}";
                input = stringtopojo(stringBody);
                //System.out.println("INPUT "+input.toString());
            }
            if(i==1){
                //System.out.println("STR1=="+str[i]);
                String ip = str[i].substring(str[i].indexOf(":")+1,str[i].length()-1);
                //System.out.println("Input STR 1 = "+ip);
                input.setInputjson(ip);
            } 
            if(i==2){
                //System.out.println("STR2=="+str[i]);
                int lastIndex=0;
                if(str[i].lastIndexOf("}")>0  ){
                    lastIndex = str[i].lastIndexOf("}");
                }else if(str[i].lastIndexOf(",")>0){
                    lastIndex = str[i].lastIndexOf(",");
                }
                //System.out.println("Last index = "+lastIndex);
                String op = str[i].substring(str[i].indexOf(":")+1,lastIndex+1) ;
                //System.out.println("Output 2 = "+op);
                if(op.length()>2){
                	input.setOutputjson(op);
                }
                
            } if(i==3){
                
                String op = str[i].substring(str[i].indexOf(":")+2,str[i].lastIndexOf("}")-1) ;
                //System.out.println("Query = "+op);
                ArrayList<String> queryList = new ArrayList<String>();
                queryList.add(op);
                input.setBeforeDBQuery(queryList);
            }
        }
        return input;
    }
    private InputAPI stringtopojo(String string) {
        InputAPI readValue=null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            readValue = mapper.readValue(string, InputAPI.class);
            //System.out.println("Inpit API "+readValue.toString());
        } catch (Exception e) {
            System.out.println("Errr "+e);
        }
        return readValue;
    }
    public String readJSON(String fileName) {

        StringBuilder strBuilder = new StringBuilder();
        BufferedReader br = null;
        FileReader fr = null;
        try {
            fr = new FileReader(fileName);
            br = new BufferedReader(fr);
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                //System.out.println(sCurrentLine);
                strBuilder.append(sCurrentLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
                if (fr != null)
                    fr.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }
        return strBuilder.toString();
    }

}
