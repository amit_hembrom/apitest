package com.games24x7.actions;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.games24x7.beans.APIReport;
import com.games24x7.beans.InputAPI;
import com.games24x7.dbquery.DBAction;

public class HTTPActions {
    private static final Logger logger = LoggerFactory.getLogger(HTTPActions.class);

    public void httpPUT(InputAPI apiObj) {

        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpPut httpPut = new HttpPut(apiObj.getUrl());
            httpPut.setEntity(new StringEntity(apiObj.getInputjson()));

            System.out.println("Executing request " + httpPut.getRequestLine());

            // Create a custom response handler
            ResponseHandler<String> responseHandler = response -> {
                int status = response.getStatusLine().getStatusCode();
                System.out.println("Response code = " + status);
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            };
            String responseBody = httpclient.execute(httpPut, responseHandler);
            System.out.println("----------------------------------------");
            System.out.println(responseBody);
            System.out.println(responseBody);
        } catch (IOException e) {
            logger.error("", e);
        }
    }

    public void thisListPut(ArrayList<InputAPI> inputValue, APIReport report) {

        for (int i = 0; i < inputValue.size(); i++) {
            thisPut(inputValue.get(i), report);
        }
    }

    public void thisListPost(ArrayList<InputAPI> inputValue, APIReport report) {

        for (int i = 0; i < inputValue.size(); i++) {
            thisPost(inputValue.get(i), report);

        }
    }

    private void thisPost(InputAPI inputAPI, APIReport report) {

        boolean statusPass = true;
        boolean bodyPass = true;
        if (inputAPI.getBeforeDBQuery() != null) {
            executeDBQuery(inputAPI);
        }
        try {
            // HTTP Post request
            String USER_AGENT = "Mozilla/5.0";
            String url = inputAPI.getUrl();
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // Setting basic post requestuser
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("Content-Type", "application/json");

            String postJsonData = inputAPI.getInputjson();

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(postJsonData);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            /*
             * System.out.println("nSending 'POST' request to URL : " + url);
             * System.out.println("Post Data : " + postJsonData);
             * System.out.println("Response Code : " + responseCode);
             */
            String outputStream = getOutputResponse(con);
            //System.out.println("Integer.parseInt(inputAPI.getStatus())  "+Integer.parseInt(inputAPI.getStatus()) 
           // 		+" con.getResponseCode() "+con.getResponseCode());
           if (Integer.parseInt(inputAPI.getStatus()) != con.getResponseCode()) {
                statusPass = false;
                report.incrementFailedCout();
                report.setFailedMessage("Status expected " + inputAPI.getStatus() + " , but found "
                        + con.getResponseCode());
            }
            if ( inputAPI.getOutputjson() !=null  && !(outputStream.equalsIgnoreCase("null")) 
            		&& inputAPI.getOutputjson().length() > 0 && outputStream.length() > 0) {
                if (!compareJSON(new JSONObject(inputAPI.getOutputjson()), new JSONObject(outputStream))) {
                    bodyPass = false;
                    report.setFailedMessage("[" + inputAPI.getTestname() + " ] Expected : "
                            + new JSONObject(inputAPI.getOutputjson()) + " , but found : "
                            + new JSONObject(outputStream));
                }
            } /*
               * else if (inputAPI.getOutputjson().length() == 0 &&
               * outputStream.length() > 0) { bodyPass = false;
               * report.setFailedMessage("[" + inputAPI.getTestname() +
               * " ] Expected : " + inputAPI.getOutputjson() + " , but found : "
               * + new JSONObject(outputStream)); ; }
               */
            //System.out.println("Status pass = "+statusPass+" , body pass = "+bodyPass);
            if (statusPass && bodyPass) {
                report.setPassedMessage(inputAPI.getTestname() + " :Passed");
                System.out.println(inputAPI.getTestname() + " :Passed");
            } else {
                report.setFailedTestCases(inputAPI.getTestname());
                System.out.println(" =========>> " + inputAPI.getTestname() + " :Failed");
                System.out.println("Status expected " + inputAPI.getStatus() + " , but found "
                + con.getResponseCode());
                System.out.println(("[" + inputAPI.getTestname() + " ] Expected : "+new JSONObject(inputAPI.getOutputjson()) + " , but found : "+new JSONObject(outputStream)));
                System.out.println("*****************************************88");
            }

        } catch (Exception e) {
            System.out.println("Exception 77 " + e);
        }

    }

    private void executeDBQuery(InputAPI inputAPI) {
        DBAction dbAction = new DBAction();
        dbAction.executeQuery(inputAPI);
    }

    public void thisPut(InputAPI apiObj, APIReport report) {
        URL url;
        boolean statusPass = true;
        boolean bodyPass = true;
        try {
            url = new URL(apiObj.getUrl());
            HttpURLConnection httpCon;
            try {
                httpCon = (HttpURLConnection) url.openConnection();
                httpCon.setDoOutput(true);
                httpCon.setRequestMethod("PUT");
                // httpCon.setRequestMethod("POST");
                OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
                out.write(apiObj.getInputjson());
                out.close();
                if (Integer.parseInt(apiObj.getStatus()) != httpCon.getResponseCode()) {
                    statusPass = false;
                    report.incrementFailedCout();
                    report.setFailedMessage("Status expected " + apiObj.getStatus() + " , but found "
                            + httpCon.getResponseCode());
                }
                String outputStream = getOutputResponse(httpCon);
                if (apiObj.getOutputjson().length() > 0 && outputStream.length() > 0) {
                    if (!compareJSON(new JSONObject(apiObj.getOutputjson()), new JSONObject(outputStream))) {
                        bodyPass = false;
                        report.setFailedMessage("[" + apiObj.getTestname() + " ] Expected : "
                                + new JSONObject(apiObj.getOutputjson()) + " , but found : "
                                + new JSONObject(outputStream));
                    }
                } else if (apiObj.getOutputjson().length() == 0 && outputStream.length() > 0) {
                    bodyPass = false;
                    report.setFailedMessage("[" + apiObj.getTestname() + " ] Expected : " + apiObj.getOutputjson()
                            + " , but found : " + new JSONObject(outputStream));
                    ;
                }
                if (statusPass && bodyPass) {
                    report.setPassedMessage(apiObj.getTestname() + " :Passed");
                    System.out.println(apiObj.getTestname() + " :Passed");
                } else {
                    report.setFailedTestCases(apiObj.getTestname());
                    System.out.println(" =========>> " + apiObj.getTestname() + " :Failed");
                }

            } catch (IOException e) {
                logger.error("", e);
            }

        } catch (MalformedURLException e) {
            logger.error("", e);
        }

    }

    private boolean compareJSON(JSONObject expectedJSON, JSONObject responseJSON) {

        // System.out.println("Response JSON  :::: "+responseJSON);
        // System.out.println("Expected JSON :::: "+expectedJSON);
        final ObjectMapper mapper = new ObjectMapper();
        JsonNode tree1;
        try {
            tree1 = mapper.readTree(expectedJSON.toString());
            final JsonNode tree2 = mapper.readTree(responseJSON.toString());
            return tree1.equals(tree2);
        } catch (JsonProcessingException e) {
            logger.error("", e);
        } catch (IOException e) {
            logger.error("", e);
        }
        return false;
    }

    private String getOutputResponse(HttpURLConnection httpCon) {
        BufferedReader br;
        StringBuilder sb = new StringBuilder();
        
        try {
           // System.out.println( " httpCon.getResponseCode() "+httpCon.getResponseCode());
            
            if (httpCon.getResponseCode() >= 200 && httpCon.getResponseCode() <= 299) {
                br = new BufferedReader(new InputStreamReader((httpCon.getInputStream())));
            } else {
                System.out.println( " httpCon.getErrorStream() "+httpCon.getInputStream());
                // br = new BufferedReader(new
                // InputStreamReader(httpCon.getInputStream()));
                br = new BufferedReader(new InputStreamReader(httpCon.getErrorStream()));
            }
            String output;
            while ((output = br.readLine()) != null) {
                sb.append(output);
            }
        } catch (IOException e) {
            logger.error("", e);
        }
        return sb.toString();
    }

    public void thisListGet(ArrayList<InputAPI> inputValue, APIReport report) {
        try {
            for (int i = 0; i < inputValue.size(); i++) {
                thisGet(inputValue.get(i), report);
            }
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    private void thisGet(InputAPI inputAPI, APIReport report) throws Exception {
        URL url;
        boolean statusPass = true;
        boolean bodyPass = true;
        try {
            url = new URL(inputAPI.getUrl());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            String outputStream = getOutputResponse(conn);
           //System.out.println(" outputStream "+outputStream.length());
           // System.out.println("Status expected " + inputAPI.getStatus() + " , Status found "
                 //       + conn.getResponseCode());
            if (Integer.parseInt(inputAPI.getStatus()) != conn.getResponseCode()) {
                statusPass = false;
                report.incrementFailedCout();
                report.setFailedMessage("Status expected " + inputAPI.getStatus() + " , but found "
                        + conn.getResponseCode());
            }
            if (inputAPI.getOutputjson()!=null && inputAPI.getOutputjson().length() > 0 && outputStream.length() > 0) {
                if (!compareJSON(new JSONObject(inputAPI.getOutputjson()), new JSONObject(outputStream))) {
                    bodyPass = false;
                    report.setFailedMessage("[" + inputAPI.getTestname() + " ] Expected : "
                            + new JSONObject(inputAPI.getOutputjson()) + " , but found : "
                            + new JSONObject(outputStream));
                }
            } else if (inputAPI.getOutputjson().length() == 0 && outputStream.length() > 0) {
                bodyPass = false;
                report.setFailedMessage("[" + inputAPI.getTestname() + " ] Expected : " + inputAPI.getOutputjson()
                        + " , but found : " + new JSONObject(outputStream));
                ;
            }
            if (statusPass && bodyPass) {
                report.setPassedMessage(inputAPI.getTestname() + " :Passed");
                System.out.println(inputAPI.getTestname() + " :Passed");
            } else {
                report.setFailedTestCases(inputAPI.getTestname());
                System.out.println(" =========>> " + inputAPI.getTestname() + " :Failed");
            }
        } catch (Exception e) {
            logger.error("Exception in get Call :: ", e);
        }

    }

}
