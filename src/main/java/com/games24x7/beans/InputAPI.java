package com.games24x7.beans;

import java.util.ArrayList;

public class InputAPI {

    private String testname;
    private String url;
    private String actiontype;
    private String status;
    private String errormessage;
    private String errorcode;
    private String dbIP;
    private String dbUser;
    private String dbPass;
    private String dbSchema;
    
    private String inputjson;
    private String outputjson;
    
    private ArrayList<String> beforeDBQuery;
    
    
    public String getTestname() {
        return testname;
    }
    public void setTestname(String testname) {
        this.testname = testname;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getActiontype() {
        return actiontype;
    }
    public void setActiontype(String actiontype) {
        this.actiontype = actiontype;
    }
    public String getInputjson() {
        return inputjson;
    }
    public void setInputjson(String inputjson) {
        this.inputjson = inputjson;
    }
    
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getErrormessage() {
        return errormessage;
    }
    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }
    public String getErrorcode() {
        return errorcode;
    }
    public void setErrorcode(String errorcode) {
        this.errorcode = errorcode;
    }
   
    public String getOutputjson() {
        return outputjson;
    }
    public void setOutputjson(String outputjson) {
        this.outputjson = outputjson;
    }
    
    public ArrayList<String> getBeforeDBQuery() {
        return beforeDBQuery;
    }
    public void setBeforeDBQuery(ArrayList<String> beforeDBQuery) {
        this.beforeDBQuery = beforeDBQuery;
    }
    
    public String getDbIP() {
        return dbIP;
    }
    public void setDbIP(String dbIP) {
        this.dbIP = dbIP;
    }
    public String getDbUser() {
        return dbUser;
    }
    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }
    public String getDbPass() {
        return dbPass;
    }
    public void setDbPass(String dbPass) {
        this.dbPass = dbPass;
    }
    public String getDbSchema() {
        return dbSchema;
    }
    public void setDbSchema(String dbSchema) {
        this.dbSchema = dbSchema;
    }
    @Override
    public String toString() {
        return "InputAPI [testname=" + testname + ", url=" + url + ", actiontype=" + actiontype + ", status=" + status
                + ", errormessage=" + errormessage + ", errorcode=" + errorcode + ", dbIP=" + dbIP + ", dbUser="
                + dbUser + ", dbPass=" + dbPass + ", dbSchema=" + dbSchema + ", inputjson=" + inputjson
                + ", outputjson=" + outputjson + ", beforeDBQuery=" + beforeDBQuery + "]";
    }
     
     
}
