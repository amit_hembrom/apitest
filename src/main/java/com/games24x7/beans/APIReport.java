package com.games24x7.beans;

import java.util.HashSet;
import java.util.TreeSet;

public class APIReport {

    
    private int failedCount;
    private int failedBodyCount;
    private TreeSet<String> failedBody= new TreeSet<String>();
    private TreeSet<String> failedTestCases= new TreeSet<String>();
    private TreeSet<String> passedTestCases= new TreeSet<String>();
    
    
    public int getFailedBodyCount() {
        return failedBodyCount;
    }
    public void setFailedBodyCount(int failedBodyCount) {
        this.failedBodyCount = failedBodyCount;
    }
    public int getFailedAssertCount() {
        return failedCount;
    }
    public void incrementFailedCout() {
        this.failedCount++;
    }
    public TreeSet<String> getFailedAsserts() {
        return failedBody;
    }
    public void setFailedMessage(String failedMessage) {
        this.failedBody.add(failedMessage);
    }
    public TreeSet<String> getPassedAsserts() {
        return passedTestCases;
    }
    public void setPassedMessage(String passedString) {
        this.passedTestCases.add(passedString);
    }
    
    public TreeSet<String> getFailedTestCases() {
        return failedTestCases;
    }
    public void setFailedTestCases( String failedTestCases) {
        this.failedTestCases.add(failedTestCases); 
    }
    @Override
    public String toString() {
        return "APIReport [failedCount=" + failedCount + ", failedBodyCount=" + failedBodyCount + ", failedBody="
                + failedBody + ", failedTestCases=" + failedTestCases + ", passedTestCases=" + passedTestCases + "]";
    }
}
