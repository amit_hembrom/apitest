package com.games24x7.beans;

import java.util.HashMap;

public class OutputAPI {
    
    private int httpStatus;
    private HashMap<Object,Object> checkValue;
    public int getHttpStatus() {
        return httpStatus;
    }
    public void setHttpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
    }
    public HashMap<Object, Object> getCheckValue() {
        return checkValue;
    }
    public void setCheckValue(HashMap<Object, Object> checkValue) {
        this.checkValue = checkValue;
    }
    
    

}
