package com.games24x7.init;

import java.util.ArrayList;

import com.games24x7.actions.HTTPActions;
import com.games24x7.actions.ReadJSON;
import com.games24x7.beans.APIReport;
import com.games24x7.beans.InputAPI;
import com.games24x7.mq.action.ExecuteMQ;

public class StartProcess {

    static String BASE_PATH = "/home/amith/workspace/apitest/apitest/src/main/resources/";

    public static void main(String[] args) {

        /*
         * User Service
         */
        // runUserServiceAPI();

        /*
         * Bonus Service
         */

        
         runBonusServiceCreateAPI(); 
         runupdatePromoCode();
         runChildBAFInfoAPI(); 
         rungetBAFBonusInfo(); 
         runParentChildMap();
         
        // dbRead();

        //runmappingParentPromocode("BAF15024884444457sasssssssseeeeeeeeeeeeeeeeeeeeeee7755444XYZ1");

    }

    private static void runmappingParentPromocode(String promoCode) {
    	
    	long userId=1200l;
    	int batch=1200;
        String userIdList= "";
        System.out.println("Starting userId " + userId + " of batch " + batch + " of threads " + 10 + "...!!");
        for (long i = userId; i < userId + batch; i++) {
                if(userIdList.length()>0){
                    userIdList=userIdList+","+i;
                }else{
                    userIdList=i+"";
                }
        }
		ExecuteMQ.publishMessage(userId, promoCode, userIdList);
		
	}

    private static void runupdatePromoCode() {

        String FILE_PATH = "updatePromoCode.json";

        ArrayList<InputAPI> inputValue = loadData(FILE_PATH);
        HTTPActions http = new HTTPActions();
        APIReport report = new APIReport();
        System.out.println("====== Report for updatePromo API =====");
        http.thisListPost(inputValue, report);

        printReport(report);

    }

    private static void runParentChildMap() {
        String FILE_PATH = "parentChildMap.json";

        ArrayList<InputAPI> inputValue = loadData(FILE_PATH);
        HTTPActions http = new HTTPActions();
        APIReport report = new APIReport();
        System.out.println("====== Report for paretnChildMap API =====");
        http.thisListPost(inputValue, report);

        printReport(report);
    }

    private static void rungetBAFBonusInfo() {
        String FILE_PATH = "getBAFBonusInfo.json";

        ArrayList<InputAPI> inputValue = loadData(FILE_PATH);
        HTTPActions http = new HTTPActions();
        APIReport report = new APIReport();
        System.out.println("====== Report for getBAFBonusInfo API =====");
        http.thisListGet(inputValue, report);

        printReport(report);
    }

    private static void runChildBAFInfoAPI() {
        String FILE_PATH = "getBAFInfoByChildId.json";

        ArrayList<InputAPI> inputValue = loadData(FILE_PATH);
        HTTPActions http = new HTTPActions();
        APIReport report = new APIReport();
        System.out.println("====== Report for childBafInfo API =====");
        http.thisListGet(inputValue, report);

        printReport(report);
    }

    private static void dbRead() {
        String FILE_PATH = "r1.txt";

        ArrayList<InputAPI> inputValue = loadData(FILE_PATH);
        HTTPActions http = new HTTPActions();
        APIReport report = new APIReport();
        http.thisListPost(inputValue, report);
        printReport(report);
    }

    private static void runBonusServiceCreateAPI() {
        String FILE_PATH = "bafCreate.json";

        ArrayList<InputAPI> inputValue = loadData(FILE_PATH);
        HTTPActions http = new HTTPActions();
        APIReport report = new APIReport();
        System.out.println("====== Report for createPromo API =====");
        http.thisListPost(inputValue, report);

        printReport(report);
    }

    private static ArrayList<InputAPI> loadData(String FILE_PATH) {
        ReadJSON read = new ReadJSON();
        String payload = read.readJSON(BASE_PATH + FILE_PATH);
        ArrayList<InputAPI> inputValue = read.jsonToInputJSON(payload);
        return inputValue;
    }

    private static void runUserServiceAPI() {
        String FILE_PATH = "59bulkupdate.json";

        ArrayList<InputAPI> inputValue = loadData(FILE_PATH);
        HTTPActions http = new HTTPActions();
        APIReport report = new APIReport();
        http.thisListPut(inputValue, report);
        printReport(report);
    }

    private static void printReport(APIReport report) {
        System.out.println("Report : " + report.toString());
        System.out.println("****************** DONE *********************");
    }
}
