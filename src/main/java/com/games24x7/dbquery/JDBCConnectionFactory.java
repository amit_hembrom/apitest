package com.games24x7.dbquery;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Vector;

public class JDBCConnectionFactory {

 
	public static Connection createConnection( String ip , String user , String pass )
	{
		   Connection connection = null;
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      /*System.out.println("Connecting to database...");
		      System.out.println("IP"+ip);
		      System.out.println("user"+user);
		      System.out.println("pass"+pass);
		      */
		      connection = DriverManager.getConnection(ip,user,pass);
		      //System.out.println("Connected.");
		     
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }
		   return connection;
	}
}
