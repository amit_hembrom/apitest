package com.games24x7.dbquery;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.games24x7.beans.InputAPI;
 
public class DBAction {
    private static final Logger logger = LoggerFactory.getLogger(DBAction.class);
    
    public void executeQuery(InputAPI inputAPI){
        ArrayList<String> queries = inputAPI.getBeforeDBQuery();
        String url = inputAPI.getDbIP() + inputAPI.getDbSchema();

        Connection conn =null;
        try{
            conn = JDBCConnectionFactory.createConnection(url, inputAPI.getDbUser(),inputAPI.getDbPass());
            for(int i=0;i<queries.size();i++){
                Statement stmt = null;
                ResultSet rs = null;
                try {
                    stmt = conn.createStatement();
                    stmt.executeUpdate(queries.get(i));
                    //System.out.println("Query 333 : "+queries.get(i)+" Done.");
                } catch (SQLException ex) {
                    System.out.println("SQLException: " + ex.getMessage());
                    System.out.println("SQLState: " + ex.getSQLState());
                    System.out.println("VendorError: " + ex.getErrorCode());
                } finally {
                    closeStatement(stmt);
                }
            }
            
        }catch (Exception e) {

            e.printStackTrace();

        }
        finally {
            closeConnection(conn);
        }
    }
    
    
    private void closeStatement(Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException sqlEx) {
            } // ignore

            stmt = null;
        }
    }


    public void closeConnection(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException sqlEx) {
            } // ignore

            conn = null;
        }
    }

   
}
