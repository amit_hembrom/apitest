package com.games24x7.mq.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author poojap
 */
public class ExecuteMQ {
    
    private static final Logger logger = LoggerFactory.getLogger(ExecuteMQ.class);

    public static int THREAD_POOL_SIZE = 1;
    private static ExecutorService executorService = null;
    private static Connection connection;
    private static Channel channel;
   // private static final String EXCHANGE_NAME ="EdsPromocodeMap";
    private static final String EXCHANGE_NAME ="EdsPromocodeMapBAF";
    private static final String ROUTING_KEY="";
    public static Channel getChannel() {
        return channel;
    }

    public static void init() {
        try {
            executorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);

            ConnectionFactory factory = new ConnectionFactory();
            //factory.setHost("10.14.24.20");
            factory.setHost("10.14.25.67");
            factory.setPort(5672);
            factory.setUsername("gwportal");
            factory.setPassword("gwportal");

            connection = factory.newConnection();
            channel = connection.createChannel();

            System.out.println("Connected on rabbitMQ...!!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void publishMessage(Long userId) {
        try {
            String reg = "{\"id\":\"14373\",\"systime\":\"%systime%\",\"dest\":\"eds\",\"source\":\"rummycircle\",\"ack\":\"0\",\"value\":\"{\\\"userId\\\":%userid%,\\\"login_id\\\":\\\"abhilashky\\\",\\\"email_id\\\":\\\"ky.abhi007@gmail.com\\\",\\\"gender\\\":\\\"M\\\",\\\"yob\\\":1998,\\\"registration_date\\\":1469625948772,\\\"landing_page_source\\\":\\\"nonstopfun.html\\\",\\\"referrer_id\\\":-1,\\\"specialoffers\\\":true,\\\"registrationSource\\\":7,\\\"referRegisteredFrom\\\":1,\\\"userPreferredLanguage\\\":1,\\\"eventType\\\":\\\"playerreg\\\",\\\"channelId\\\":%channelid%,\\\"utmSource\\\":\\\"test1\\\",\\\"utmCampaign\\\":\\\"test1\\\",\\\"utmTerm\\\":\\\"term\\\",\\\"utmMedium\\\":\\\"test1\\\",\\\"utmContent\\\":\\\"test1\\\"}\",\"type\":\"playerreg\"}";
            String login = "{\"id\":\"4\",\"systime\":\"%systime%\",\"dest\":\"eds\",\"source\":\"rummycircle\",\"ack\":\"0\",\"value\":\"{\\\"userId\\\":%userid%,\\\"lastLoginDate\\\":%lastlogindate%,\\\"userPreferredLanguage\\\":2,\\\"eventType\\\":\\\"playerlogin\\\",\\\"channelId\\\":1,\\\"utmSource\\\":\\\"ABCS\\\"}\",\"type\":\"playerlogin\"}";
            String event = login;

            event = event.replaceAll("%systime%", System.currentTimeMillis()
                    + "");

            String msg = event;

            msg = msg.replaceAll("%userid%", userId + "");
            msg = msg.replaceAll("%channelid%", 1 + "");
            msg = msg.replaceAll("%lastlogindate%",
                    System.currentTimeMillis() + "");

            System.out.println("message==> " + msg);

            EventPublisherTopicTask eventPublisherTask = new EventPublisherTopicTask(
                   EXCHANGE_NAME, ROUTING_KEY, msg,
                    getChannel());
            executorService.submit(eventPublisherTask);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void publishMessage(Long userId, String promoCode,String userIdString) {
        try {
            init();
            String msg = "{\"source\": \"EDS\",\"campaignId\": \"59db12c6cec74e19889d3777\",\"publishTime\":1510761475243,\"promoCode\": \""+promoCode+"\",\"users\": ["+userIdString+"]}";
            System.out.println("messSystem.out.println ==> "+msg);

            EventPublisherTopicTask task = new EventPublisherTopicTask(
                    EXCHANGE_NAME, ROUTING_KEY, msg,
                    getChannel());
            executorService.submit(task);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void close() {
        try {
            Thread.sleep(1000);
            if (channel != null && channel.isOpen()) {
                channel.close();
            }
            if (connection != null && connection.isOpen()) {
                connection.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void closeThreadPool() {
        try {
            System.out.println("Closing Connection...!!");
            executorService.shutdown();
            while (!executorService.isTerminated()) {
            }
            if (executorService.isTerminated()) {
                close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    public static void main(String[] args) {
        try {
            long batch = 12000;
            Long userId = 1523L;
            if (null != args && args.length > 0) {
                userId = Long.parseLong(args[0]);
                batch = Long.parseLong(args[1]);
                THREAD_POOL_SIZE = Integer.parseInt(args[2]);
            }

            init();
            String userIdList= "";
            System.out.println("Starting userId " + userId + " of batch " + batch + " of threads " + THREAD_POOL_SIZE + "...!!");
            for (long i = userId; i < userId + batch; i++) {
                    if(userIdList.length()>0){
                        userIdList=userIdList+","+i;
                    }else{
                        userIdList=i+"";
                    }
//                publishMessage(i, "playerreg");
//                publishMessage(i, "playerlogin");
//                publishMessage(i, "firstdeposit");
//                publishMessage(i, "currentwithdrawable");
//                publishMessage(i, "bonusgrant");
//                publishMessage(i, "accountstatus");
//                publishMessage(i, "currentwithdrawable");
//                publishMessage(i, "bonusdetailsupdate");
//                publishMessage(i, "bafbonusreleased");
//                publishMessage(i, "gameplayed");
//                publishMessage(i, "updateclubandloyalty");
//                publishMessage(i, "rpUpdate");
//                publishMessage(i, "bonusunitreserve");
//                publishMessage(i, "appinstall");
//                publishMessage(i, "appopen");
//                publishMessage(i, "txnFail");
//                publishMessage(i, "repeatAddCash");
//                publishMessage(i, "WebPushEnable");
//                publishMessage(i, "GcmDeviceReg");
//                publishMessage(i, "PlayerDetailUpdate");
//                publishMessage(i, "tournamentjoin");
//                publishMessage(i, "bonusgrant");
            }
            publishMessage(211l, "BAF15012-122",userIdList);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeThreadPool();
        }
    }
}