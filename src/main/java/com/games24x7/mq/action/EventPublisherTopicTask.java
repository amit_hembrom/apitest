package com.games24x7.mq.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.Channel;

/**
 *
 * @author poojap
 */
public class EventPublisherTopicTask implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(EventPublisherTopicTask.class);
    private String exchange, routingkey, message;
    private Channel channel;

    public EventPublisherTopicTask(String exchange, String routingkey,
            String message, Channel channel) {
        this.exchange = exchange;
        this.routingkey = routingkey;
        this.message = message;
        this.channel = channel;
    }

    @Override
    public void run() {
        try {
            // System.out.println("msg==> " + message);
            channel.basicPublish(exchange, routingkey, null, message.getBytes("UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}